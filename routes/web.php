<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('website.frontend.layouts.main');
// });

Auth::routes();

Route::get('/', 'FrontendController@index')->name('website.index');
Route::get('/add-to-cart/{id}', 'FrontendController@addToCart')->name('website.addCart');
Route::get('/shopping-cart', 'FrontendController@getCart')->name('website.cart');
Route::get('/checkout-cart', 'FrontendController@getCheckout')->name('website.checkout');
Route::post('/checkout-cart', 'FrontendController@storeOrder')->name('website.storeOrder');




Route::get('/home', 'HomeController@index')->name('home');


// admin----------------------------------------------------------------------------------------------------------------
Route::group(['prefix' => 'dashboard', 'middleware' => ['auth','admin']], function () {

Route::get('/', 'BackendController@index')->name('backend.index');

Route::resource('/productcategory','ProductCategoryController');

Route::resource('/product','ProductController');
Route::resource('/productImage','ProductImageController');

Route::resource('/customerDetail','CustomerDetailController');
Route::resource('/payment','PaymentController');

Route::resource('/contact','ContactController');
Route::resource('/contactForm','ContactFormController');
Route::resource('/image','ImageController');


});

