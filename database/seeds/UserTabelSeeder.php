<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\Hash;

class UserTabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
                    [
                        'email'     => 'admin@gmail.com',
                        'password'  => Hash::make('123456789'),
                        'name'      => 'admin',
                        'role'      => User::ADMIN,
                        'adress'   => 'Address-1',
                        'phone' => '5555555555',
                    ], [
                        'email'     => 'user@gmail.com',
                        'password'  => Hash::make('123'),
                        'name'      => 'user',
                        'role'      => User::USER,
                        'adress'   => 'Address-2',
                        'phone' => '5555555555',
                    ]
    }
}
